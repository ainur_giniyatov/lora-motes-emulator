from distutils.core import setup

# build with "python -m build"

setup(name='lora-motes-emulator',
      version='1.0',
      description='emulate lora node and gateway',
      packages=['motes'],
      requires=['pycryptodome', 'pyyaml']
      )
